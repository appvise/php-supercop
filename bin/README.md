# Binary inclusions

These binaries have been compiled from the
[finwo/supercop-cli](https://github.com/finwo/supercop-cli) project with the
following command on their respective architecture:

```sh
make static CC=clang MARCH=<ARCH> MTUNE=generic
```

Basically, the linux-x86_64 version has been compiled on a linux-x86_64 machine.
