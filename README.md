# appvise/supercop

Helper library for handling supercop-ref10 ed25519 digital signatures

## Heads-up

This library makes use of a compiled command-line version of
[orlp/ed25519](https://github.com/orlp/ed25519), namely
[finwo/supercop-cli](https://github.com/finwo/supercop-cli). It uses this
command-line utility through the `exec` function, which may have been blocked by
your hosting provider.

## Work-in-progress

This library is still a work-in-progress and not all features you may expect
have been implemented yet.

TODO:

- Shared-secret generation between different keys (a.k.a. key exchange)
- Loading a public-only key

## API

```php

use Appvise\Supercop\Key;

// Generate or keys for this example
$keypair = Key::generate();
$keypair = Key::fromFile("/path/to/key/file");

// Generate a signature
$signature = $keypair->sign("hello world");

// Signature validation
var_dump($keypair->verify("hello world", $signature   )); // true
var_dump($keypair->verify("HELLO WORLD", $signature   )); // false
var_dump($keypair->verify("hello-world", $signature   )); // false
var_dump($keypair->verify("hello world", "invalid-sig")); // false
var_dump($keypair->verify("hello world", "invalid-sig")); // false
```
