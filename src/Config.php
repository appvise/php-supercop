<?php

namespace Appvise\Supercop;

class Config {

    // Storage location for the data
    private static $cfg = array();

    /**
     * @param array $values  Associative array with data to insert into the supercop config
     *
     * @return void
     */
    static function load($values) {
        array_replace(self::$cfg, $key);
        return;
    }

    /**
     * @param string $key    Name of the value to overwrite
     * @param mixed  $value  Value to insert at the given key
     *
     * @return void
     */
    static function set($key, $value) {
        self::$cfg[$key] = $value;
    }

    /**
     * @param string $key  Name of the value to retreive
     *
     * @return mixed
     */
    static function get($key) {
        return self::$cfg[$key];
    }

    /**
     * Well-known configuration bin
     */
    static function executable() {
        $path = self::get('executable');

        // Attempt pre-compiled version
        if (!isset($path)) {
            $os   = strtolower(php_uname('s'));
            $arch = strtolower(php_uname('m'));
            $path = implode(DIRECTORY_SEPARATOR, [
                __DIR__,
                "..",
                "bin",
                "supercop-${os}-${arch}"
            ]);
            if (!is_executable($path)) {
                throw new \Exception("appvise/supercop does not support ${os}-${arch}");
            }
        }

        if (!is_executable($path)) {
            throw new \Exception("Given 'executable' is not something we can execute in appvise/supercop");
        }

        return $path;
    }

}
