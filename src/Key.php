<?php

namespace Appvise\Supercop;

class Key {
    public $public_key  = null;
    public $private_key = null;

    /**
     * @param string $public_key   Hex-encoded public key
     * @param string $private_key  Hex-encoded private key
     * @throws \Exception
     */
    public function __construct($public_key, $private_key) {
        if (!is_string($public_key)) {
            throw new \Exception("Given public-key must be a string");
        }

        // TODO:
        // - check if public_key is hex-only
        // - check if private_key is string (only when given)
        // - check if private_key is hex-only (only when given)

        $this->public_key  = $public_key;
        $this->private_key = $private_key;
    }

    /**
     * Loads a key from file
     *
     * @param string $path  The path to load the key from
     *
     * @throws \Exception
     * @return Key
     */
    public static function fromFile($path) {
        if (!is_file($path)) throw new \Exception("File '${path}' does not exists");
        $lines  = [];
        $safe   = escapeshellarg($path);
        $last   = exec(Config::executable() . " --printkey --key-file '${safe}'", $lines);
        $raw    = implode("\r\n", $lines) . "\r\n";
        $parsed = Helper::parse_headers($raw);
        if (!isset($parsed['public-key'])) throw new \Exception("Could not read key file: '${path}'");
        return new Key($parsed['public-key'], $parsed['private-key']);
    }

    protected static function mkdir($path) {
        if (is_dir($path)) return true;
        return mkdir($path, 0777, true);
    }

    /**
     * Stores a key into a file
     *
     * Uses the 0x00 format
     *
     * @param string $path  The path to store the key into
     *
     * @throws \Exception
     */
    public function toFile($path) {
        if (!self::mkdir(dirname($path))) throw new \Exception("Could not create directory for '${path}'");
        $fd = fopen($path, "w");
        if (!fd) throw new \Exception("'${path}' could not be opened for writing");
        fwrite($fd, hex2bin("00")              ,  1); // Writing the 0x00-type keyfile
        fwrite($fd, hex2bin($this->public_key) , 32); // Which contains the 32-byte public key un-encoded
        fwrite($fd, hex2bin($this->private_key), 64); // Then the 64-byte private key un-encoded
        fclose($fd);
    }

    /**
     * Generates a new key
     *
     * @throws \Exception
     * @return Key
     */
    public static function generate() {
        $filename = tempnam(sys_get_temp_dir(), 'supercop-key-');
        $path     = escapeshellarg($filename);
        exec(Config::executable() . " --generate --key-file '${path}'");
        $key      = self::fromFile($filename);
        unlink($filename);
        return $key;
    }

    /**
     * Creates a temporary key file
     *
     * @return string
     */
    public function tempFile() {
        $filename = tempnam(sys_get_temp_dir(), 'supercop-key-');
        $this->toFile($filename);
        return $filename;
    }

    /**
     * Signs a message using this key, to allow verification later
     *
     * @param string $message  Message to sign using this key
     *
     * @return string  Hex-encoded 64-byte signature
     */
    public function sign($message) {
        $tmpfile = $this->tempFile();
        $path    = escapeshellarg($tmpfile);
        $message = escapeshellarg($message);
        $result = exec(Config::executable() . " --sign --key-file ${path} --message ${message}");
        unlink($tmpfile);
        return $result;
    }

    /**
     * Verifies a message signature using this key
     *
     * @param string $message    The message to verify the signature agains
     * @param string $signature  The signature to verify
     *
     * @return boolean
     */
    public function verify($message, $signature) {
        $tmpfile = $this->tempFile();
        $path      = escapeshellarg($tmpfile);
        $message   = escapeshellarg($message);
        $signature = escapeshellarg($signature);
        $lines     = [];
        $result    = false;
        exec(Config::executable() . " --verify --key-file ${path} --message ${message} --signature ${signature}", $lines, $result);
        unlink($tmpfile);
        return !$result;
    }

}
